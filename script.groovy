def buildJar(){
    echo "building the application..."
    sh 'mvn package'
}
def buildImage(){
     echo "building the docker image of branch $BRANCH_NAME..."
        withCredentials([usernamePassword(credentialsId:'dockerhub-credentials',passwordVariable:'PASS',usernameVariable:'USER' )]){
            sh 'docker build -t binayr/demo-app:jma-2.1.0 .'
            sh "echo $PASS | docker login -u $USER --password-stdin"
            sh 'docker push binayr/demo-app:jma-2.1.0'
        }
}
def deploy(){
    echo "deploying the application..."
}

return this
